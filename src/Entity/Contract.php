<?php

namespace App\Entity;

use App\Repository\ContractRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContractRepository::class)
 */
class Contract
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="float")
     */
    private $hours;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endAt;

    /**
     * @ORM\Column(type="float")
     */
    private $consumed;

    /**
     * @ORM\OneToMany(targetEntity=Intervention::class, mappedBy="contract")
     */
    private $interventions;

    /**
     * @ORM\Column(type="integer")
     */
    private $honorary;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="contracts")
     */
    private $customer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $giftHours;


    public function __construct()
    {
        $this->interventions = new ArrayCollection();
    }

    public function __toString()
    {
        return "Contrat numéro " . $this->getId() . " du client " . $this->getCustomer() . " - " . $this->getCreatedAt()->format('Y') . " prenant fin le " . $this->getEndAt()->format('d/m/Y');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getHours(): ?float
    {
        return $this->hours;
    }

    public function setHours(float $hours): self
    {
        $this->hours = $hours;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getConsumed(): ?float
    {
        return $this->consumed;
    }

    public function setConsumed(float $consumed): self
    {
        $this->consumed = $consumed;

        return $this;
    }


    /**
     * @return Collection|Intervention[]
     */
    public function getInterventions(): Collection
    {
        return $this->interventions;
    }

    public function addIntervention(Intervention $intervention): self
    {
        if (!$this->interventions->contains($intervention)) {
            $this->interventions[] = $intervention;
            $intervention->setContract($this);
        }

        return $this;
    }

    public function removeIntervention(Intervention $intervention): self
    {
        if ($this->interventions->contains($intervention)) {
            $this->interventions->removeElement($intervention);
            // set the owning side to null (unless already changed)
            if ($intervention->getContract() === $this) {
                $intervention->setContract(null);
            }
        }

        return $this;
    }

    public function getHonorary(): ?int
    {
        return $this->honorary;
    }

    public function setHonorary(int $honorary): self
    {
        $this->honorary = $honorary;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getGiftHours(): ?int
    {
        return $this->giftHours;
    }

    public function setGiftHours(?int $giftHours): self
    {
        $this->giftHours = $giftHours;

        return $this;
    }


}
