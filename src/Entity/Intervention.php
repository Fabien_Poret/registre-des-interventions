<?php

namespace App\Entity;

use App\Repository\InterventionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InterventionRepository::class)
 */
class Intervention
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $interventionAt;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="interventions")
     */
    private $customer;

    /**
     * @ORM\Column(type="float")
     */
    private $durationInHours;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $intervention;

    /**
     * @ORM\ManyToOne(targetEntity=Contract::class, inversedBy="interventions")
     */
    private $contract;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInterventionAt(): ?\DateTimeInterface
    {
        return $this->interventionAt;
    }

    public function setInterventionAt(\DateTimeInterface $interventionAt): self
    {
        $this->interventionAt = $interventionAt;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getDurationInHours(): ?float
    {
        return $this->durationInHours;
    }

    public function setDurationInHours(float $durationInHours): self
    {
        $this->durationInHours = $durationInHours;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIntervention(): ?string
    {
        return $this->intervention;
    }

    public function setIntervention(?string $intervention): self
    {
        $this->intervention = $intervention;

        return $this;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(?Contract $contract): self
    {
        $this->contract = $contract;

        return $this;
    }
}
