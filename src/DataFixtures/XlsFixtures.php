<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class XlsFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        
        $arrayCustomers = array(
            0 => 'MMH',
            1 => 'Morny',
            2 => 'Hotel Normandie',
            3 => 'Joigneaux-Pattes',
            4 => 'Apittera',
            5 => 'PIC',
        );

        foreach ($arrayCustomers as $key => $arrayCustomer) {
            $customer = new Customer();
            $customer->setName($arrayCustomer);
            $customer->setSince(new DateTime());
            $manager->persist($customer);
        }

        $manager->flush();


    }
}
