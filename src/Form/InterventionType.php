<?php

namespace App\Form;

use App\Entity\Contract;
use App\Entity\Customer;
use App\Entity\Intervention;
use Doctrine\ORM\EntityRepository;
use App\Repository\ContractRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class InterventionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('interventionAt', DateTimeType::class, [
                'label' => 'La date de votre intervention',
                'widget' => 'single_text',
            ])          
            ->add('durationInHours', NumberType::class, [
                'label' => 'La durée de votre intervention (en heure)',
            ])            
            ->add('type', ChoiceType::class, [
                'label' =>  "Le type d'intervention",
                'choices' => [
                    'Distant' => 'Distant',
                    'Sur site' => 'Sur site'
                ]
            ])
            ->add('intervention', TextareaType::class, [
                'label' => 'Description de l\'intervention'
            ])
            ->add('contract', EntityType::class,[
                'class'=> Contract::class,
                'label' => 'Le contrat en relation avec l\'intervention',
                'placeholder' => 'Choix du contrat',
                'query_builder' => function (EntityRepository  $er) {
                    return $er->createQueryBuilder('contract')
                        ->Where('contract.endAt > CURRENT_DATE()');
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Intervention::class,
        ]);
    }
}
