<?php

namespace App\Form;

use App\Entity\Contract;
use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class ContractType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('createdAt')
            ->add('hours', IntegerType::class, [
                'label' => 'La base horaire de votre contrat (en heure)',
            ])     
            ->add('giftHours', IntegerType::class, [
                'label' => 'Le nombre d\'heures offertes',
                'data' => '0'
            ])    
            ->add('honorary', IntegerType::class, [
                'label' => 'Le prix par heure (en euros)',
                'data' => '48'
            ])     
            ->add('endAt', DateTimeType::class, [
                'label' => 'La date de fin de votre contrat',
                'widget' => 'single_text',
                // 'input'  => 'datetime_immutable',
                // 'attr' => ['class' => 'form-control'],
            ])          
            // ->add('consumed')
            ->add('customer',EntityType::class,[
                'class'=>Customer::class,
                'label' => 'Le client du contrat',
                'choice_label' => 'name',
                'placeholder' => 'Choix du client',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contract::class,
        ]);
    }
}
