<?php

namespace App\Controller;

use Dompdf\Dompdf;
use App\Data\SearchData;
use App\Form\SearchType;
use App\Entity\Intervention;
use App\Form\InterventionType;
use App\Repository\InterventionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
/**
 * @Route("/intervention")
 */
class InterventionController extends AbstractController
{
    /**
     * @Route("/", name="intervention_index", methods={"GET", "POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(InterventionRepository $interventionRepository, Request $request): Response
    {
        $data = new SearchData();
        $data->page = $request->get('page', 1);
        $form = $this->createForm(SearchType::class, $data);
        
        $form->handleRequest($request);
        $interventions = $interventionRepository->findSearch($data);

        return $this->render('intervention/index.html.twig', [
            'interventions' => $interventions,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/new", name="intervention_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $intervention = new Intervention();
        $form = $this->createForm(InterventionType::class, $intervention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $intervention->setCustomer($intervention->getContract()->getCustomer());
            $contract = $intervention->getContract();
            $interventionsInHours = $contract->getConsumed();
            $contract->setConsumed($interventionsInHours + $intervention->getDurationInHours());

            $entityManager->persist($intervention);
            $entityManager->persist($contract);
            $entityManager->flush();
            $this->addFlash('success', 'Votre intervention à bien été créée');
            return $this->redirectToRoute('intervention_index');
        }

        return $this->render('intervention/new.html.twig', [
            'intervention' => $intervention,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="intervention_show", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function show(Intervention $intervention): Response
    {
        return $this->render('intervention/show.html.twig', [
            'intervention' => $intervention,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="intervention_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, Intervention $intervention): Response
    {
        $form = $this->createForm(InterventionType::class, $intervention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $contract = $intervention->getContract();
            $consumed = $contract->getConsumed();
            $contract->setConsumed($intervention->getDurationInHours());
            $entityManager->persist($contract);

            $entityManager->persist($intervention);
            $entityManager->flush();
            $this->addFlash('info', 'Votre intervention à bien été modifié');
            return $this->redirectToRoute('intervention_index');
        }

        return $this->render('intervention/edit.html.twig', [
            'intervention' => $intervention,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="intervention_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete(Request $request, Intervention $intervention): Response
    {
        if ($this->isCsrfTokenValid('delete'.$intervention->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            $contract = $intervention->getContract();
            $consumed = $contract->getConsumed();
            $contract->setConsumed($consumed - $intervention->getDurationInHours());
            $entityManager->persist($contract);

            $entityManager->remove($intervention);
            $entityManager->flush();
            $this->addFlash('warning', 'Votre intervention à bien été supprimé');
        }

        return $this->redirectToRoute('intervention_index');
    }

    /**
     * @Route("/generate/{id}", name="intervention_generate", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function generateInvoice(Intervention $intervention): Response
    {
        // TODO: Mettre en page la facture

        $dompdf = new Dompdf();
        $dompdf->loadHtml('
            <h1>' . $intervention->getCustomer() . '</h1>
        ');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();

        return $this->render('_parts/_pdf.html.twig', [
        ]);
    }
}
