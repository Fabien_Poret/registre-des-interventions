<?php

namespace App\Controller;

use DateTime;
use Dompdf\Dompdf;
use App\Entity\Customer;
use App\Form\CustomerType;
use App\Repository\CustomerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
/**
 * @Route("/customer")
 */
class CustomerController extends AbstractController
{
    /**
     * @Route("/", name="customer_index", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(CustomerRepository $customerRepository): Response
    {
        
        return $this->render('customer/index.html.twig', [
            'customers' => $customerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="customer_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $dateTime = new DateTime();
            $customer->setSince($dateTime);
            $entityManager->persist($customer);
            $entityManager->flush();
            $this->addFlash('success', 'Votre client à bien été créé');
            return $this->redirectToRoute('customer_index');
        }

        return $this->render('customer/new.html.twig', [
            'customer' => $customer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="customer_show", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function show(Customer $customer): Response
    {
        $dateTime = new DateTime();
        return $this->render('customer/show.html.twig', [
            'dateTime' => $dateTime,
            'customer' => $customer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="customer_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, Customer $customer): Response
    {
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('info', 'Votre client à bien été modifié');
            return $this->redirectToRoute('customer_index');
        }

        return $this->render('customer/edit.html.twig', [
            'customer' => $customer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="customer_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete(Request $request, Customer $customer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$customer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            foreach($customer->getContracts() as $contract){
                $entityManager->remove($contract);
            }
            foreach($customer->getInterventions() as $intervention){
                $entityManager->remove($intervention);
            }
            $entityManager->remove($customer);
            $entityManager->flush();
            $this->addFlash('warning', 'Votre client à bien été supprimé');
        }

        return $this->redirectToRoute('customer_index');
    }

    /**
     * @Route("/generate/{id}", name="customer_generate", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function generateInvoice(Customer $customer): Response
    {
        // TODO: Mettre en page la facture

        $dompdf = new Dompdf();
        $dompdf->loadHtml('
            <h1>' . $customer->getName() . '</h1>
        ');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();

        return $this->render('_parts/_pdf.html.twig', [
        ]);
    }
}
