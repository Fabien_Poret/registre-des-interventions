<?php

namespace App\Controller;

use DateTime;
use Dompdf\Dompdf;
use App\Entity\Contract;
use App\Form\ContractType;
use App\Repository\ContractRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
/**
 * @Route("/contract")
 */
class ContractController extends AbstractController
{
    /**
     * @Route("/", name="contract_index", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(ContractRepository $contractRepository): Response
    {
        $contracts = $contractRepository->findContractValide();
        $account = 0;
        foreach ($contracts as $contract) {
            $account = $account + $contract->getHours() * $contract->getHonorary();
        }
        return $this->render('contract/index.html.twig', [
            'account' => $account,
            'past' => false,
            'contracts' => $contracts,
        ]);
    }

    /**
     * @Route("/past", name="contract_past", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function contractPast(ContractRepository $contractRepository): Response
    {
        return $this->render('contract/index.html.twig', [
            'account' => 0,
            'past' => true,
            'contracts' => $contractRepository->findContractNotValide(),
        ]);
    }


    /**
     * @Route("/new", name="contract_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $contract = new Contract();
        $form = $this->createForm(ContractType::class, $contract);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $date = new DateTime();
            $contract->setCreatedAt($date);
            $contract->setConsumed(0);
            $this->addFlash('success', 'Votre contrat à bien été crée');
            $entityManager->persist($contract);
            $entityManager->flush();

            return $this->redirectToRoute('contract_index');
        }

        return $this->render('contract/new.html.twig', [
            'contract' => $contract,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contract_show", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function show(Contract $contract): Response
    {
        return $this->render('contract/show.html.twig', [
            'contract' => $contract,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="contract_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, Contract $contract): Response
    {
        $form = $this->createForm(ContractType::class, $contract);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('info', 'Votre contrat à bien été modifié');
            return $this->redirectToRoute('contract_index');
        }

        return $this->render('contract/edit.html.twig', [
            'contract' => $contract,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contract_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete(Request $request, Contract $contract): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contract->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            foreach($contract->getInterventions() as $intervention){
                $entityManager->remove($intervention);
            }

            $entityManager->remove($contract);
            $entityManager->flush();
            $this->addFlash('warning', 'Votre contrat à bien été supprimé');
        }

        return $this->redirectToRoute('contract_index');
    }

    /**
     * @Route("/generate/{id}", name="contract_generate", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function generateInvoice(Contract $contract): Response
    {
        // TODO: Mettre en page la facture

        $dompdf = new Dompdf();
        $dompdf->loadHtml('
            <h1>' . $contract->getCustomer() . '</h1>
        ');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();

        return $this->render('_parts/_pdf.html.twig', [
        ]);
    }
}
